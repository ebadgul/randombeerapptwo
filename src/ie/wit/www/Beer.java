package ie.wit.www;

public class Beer {

	public String name;
	public String desc;
	public double ABV;
	public String locaiton;

	public Beer() {

	}

	public Beer(String name, String desc, double ABV, String location) {
		this.name = name;
		this.desc = desc;
		this.ABV = ABV;
		this.locaiton = location;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public double getABV() {
		return ABV;
	}

	public String getLocaiton() {
		return locaiton;
	}

}
