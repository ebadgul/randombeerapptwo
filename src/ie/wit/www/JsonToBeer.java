package ie.wit.www;

import java.io.FileReader;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonToBeer {

	public JsonToBeer() {

	}

	public Beer getRandomBeer() {

		JSONParser parser = new JSONParser();
		Beer beer = new Beer();
		int x = 0;

		try {

			Object obj = parser
					.parse(new FileReader("/home/ebad/workspace-eclipse/randomBeerApp1/src/ie/wit/www/beers.json"));
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray beerList = (JSONArray) jsonObject.get("beerlist");

			Random r = new Random();
			x = r.nextInt(beerList.size());

			JSONObject jbeer = (JSONObject) beerList.get(x);
			System.out.print(jbeer);
			beer.name = (String) jbeer.get("Name");
			beer.desc = (String) jbeer.get("Desc");
			beer.ABV = (double) jbeer.get("ABV");
			beer.locaiton = (String) jbeer.get("Location");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return beer;
	}

}
